from .db_operations import initdb_pool

ADMIN_DB_CONF = {
	'host' : '172.16.16.248',
    'port' : '5432',
	'database' : 'admin',
	'user' : 'postgres',
	'password' : 'postgres'
}

TRANSPORT_DB_CONF = {
	'host' : '172.16.16.248',
    'port' : '5432',
	'database' : 'transport',
	'user' : 'postgres',
	'password' : 'postgres'
}

admindb_pool_obj = initdb_pool(ADMIN_DB_CONF)

transportdb_pool_obj = initdb_pool(TRANSPORT_DB_CONF)