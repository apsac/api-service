import psycopg2
from psycopg2 import pool, extras
from multiprocessing.pool import ThreadPool 

# Postgres Connection Pool
def initdb_pool(db_config): 
	try:
		db_pool = psycopg2.pool.ThreadedConnectionPool(5, 50, host=db_config['host'], database=db_config['database'], user=db_config['user'], password=db_config['password'], cursor_factory=psycopg2.extras.RealDictCursor)#, connect_timeout=3)
		pool_object = {
			"pool" : db_pool,
			"config" : db_config
		}
		print("PostgreSQL Connection pool created with Database: ", db_config['database'], " at host ", db_config['host'], "successfully")
		return pool_object
	except (Exception, psycopg2.DatabaseError) as error :
	    print ("Error while connecting to PostgreSQL database: ", db_config, " | Error:", error, "   |   ", psycopg2.DatabaseError)

def get_tested_pool(db_pool_obj): 
	try:
		db_pool = db_pool_obj["pool"]
		conn = db_pool.getconn()
		cur = conn.cursor()
		query = "select 1 as test"
		cur.execute(query)
		data = cur.fetchone()
		cur.close()
		try:
			db_pool.putconn(conn)
		except Exception as e:
			print(e)
		return db_pool
	except:
		print("Reconnecting to the pool with config", db_pool_obj["config"])
		db_pool_obj = initdb_pool(db_pool_obj["config"])
		return get_tested_pool(db_pool_obj)
	
def get_features():
	pass
	
def querydb(query, db_pool_obj):
	try:
		db_pool = db_pool_obj["pool"]
		conn = get_tested_pool(db_pool_obj).getconn()
		cur = conn.cursor()
		cur.execute(query)
		data = cur.fetchall()
		cur.close()
		try:
			db_pool.putconn(conn)
		except Exception as e:
			print(e)
		return data
	except Exception as e:
		rollback_db(db_pool_obj)
		raise Exception(e)

def execdb(query, db_pool_obj, key=None):
	try:
		db_pool = db_pool_obj["pool"]
		conn = get_tested_pool(db_pool_obj).getconn(key)
		cur = conn.cursor()
		cur.execute(query)
		conn.commit()
		cur.close()
		try:
			db_pool.putconn(conn)
		except Exception as e:
			print(e)
		return True
	except Exception as e:
		rollback_db(db_pool_obj)
		raise Exception(e)

 # Use rollback_db if you get any transaction related error
def rollback_db(db_pool_obj):
	db_pool = db_pool_obj["pool"]
	conn = get_tested_pool(db_pool_obj).getconn()
	cur = conn.cursor()
	cur.execute("ROLLBACK")
	conn.commit()
	cur.close()
	try:
		db_pool.putconn(conn)
	except Exception as e:
		print(e)
    
def showalltables(db_pool_obj):
    query = 'SELECT * FROM pg_catalog.pg_tables'
    data = querydb(query, db_pool_obj)
    tables = []
    for i in data:
        if i['schemaname'] == 'public':
            tables.append(i['tablename'])
            #print(i['tablename'])
    return tables


threadpool_query = ThreadPool(100)
def querydb_multi(querylist, db_pool_obj):
	data_list = []
	results = threadpool_query.imap(querydb, querylist)
	for i, j in enumerate(results):
		data_list.append(j)
	#threadpool_query.close()
	#threadpool_query.terminate()
	return data_list
