from django.urls import path
from django.conf.urls import url, include
from rest_framework.documentation import include_docs_urls

from . import views  

urlpatterns = [
    #APIs
    url(r'^test/$', views.test.as_view()), 
    url(r'^geocode/$', views.geocode.as_view()), 
    url(r'^reverse_geocode/$', views.reverse_geocode.as_view()), 

    #url(r'^list_all_tables/$', views.list_all_tables.as_view()),     
    #url(r'^renovatio_building_report/$', views.renovatio_building_report.as_view(), name='raenovatio_building_report'),

    # Documentation
    url('', include_docs_urls(title='APSAC Api Services'))
]
