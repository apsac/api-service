from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from django.contrib.auth.models import User

from common.databases import *

import pandas as pd


# Create your views here.

class geocode(APIView):
    def get(self, request):
        q = self.request.query_params.get('query', None) 
        bounds = self.request.query_params.get('bounds', None)

        response_to_send = {
            "message" : "API works - Sandeep"
        }

        if q is not None:
            search_terms = q.split()
            geocoded_data = search_terms
            response_to_send["data"] = geocoded_data
        else:
            response_to_send["usage"] = "?q=searchterms&bounds=minX,minY,maxX,maxY"

        return Response(response_to_send)


class reverse_geocode(APIView):
    def get(self, request):
        # All variables declare here
        xy = None
        x_y = []
        rg_list = []

        xy = self.request.query_params.get('xy', None)

        response_to_send = {
            "message" : "API works - Sandeep"
        }

        if xy is not None:
            x_y = xy.split('|')
        
        #print(xy)
        
        if len(x_y) > 0:
            for a in x_y:
                if len(a.split(',')) == 2:
                    wkt_ = f" POINT({a.replace(',', ' ')}) "
                    rg_list.append(wkt_)
                else:
                    response_to_send["error"] = f"syntax error near {a}"
                    response_to_send["usage"] = "the API should be supplied with parameters ?xy=X;Y,"
                    return Response(response_to_send)

        
        #print(rg_list)
        if len(rg_list) > 0:
            wkt_to_postgres = " $$$ ".join(rg_list)

            # Query Administrative Database
            admin_query = f"""
                    SELECT 
                        a.row_no as input_id,
                        ST_X(a.geom) as X,
                        ST_Y(a.geom) as Y,
                        d_name as district_name,
                        d_code as district_code,
                        m_name as subdistrict_name,
                        m_code as subdistrict_code,
                        v_name as village_name,
                        v_code as village_code
                    FROM
                        public.ap_village_boundary,
                        (   
                            SELECT 
                                ROW_NUMBER() OVER () as row_no,
                                t.geom
                            FROM 
                                (SELECT 
                                    ST_GeomFromText(unnest(string_to_array('{wkt_to_postgres}', '$$$')), 4326) AS geom
                                ) t
                        ) a
                    WHERE ST_Intersects(a.geom, wkb_geometry)
                    ;
                    """
            admin_data = querydb(admin_query, admindb_pool_obj)
            a_df = pd.DataFrame(admin_data).set_index('input_id')

            # Query Transport Database
            nearest_road_query = f"""
                    SELECT 
                        e.row_no as input_id,
                        e.rb_no as rb_no,
                        e.rd_sub_typ as road_type,
                        e.distance as dist_nearest_road_m
                    FROM
                        (SELECT d.*,
                            RANK() OVER(
                                PARTITION BY d.row_no
                                ORDER BY d.distance
                            )
                        FROM
                            (SELECT 
                                roads.*, 
                                ST_Distance( a.geom, roads.wkb_geometry ) as distance,
                                a.row_no,	
                                a.geom
                            FROM
                                (   
                                    SELECT 
                                        ROW_NUMBER() OVER () as row_no,
                                        t.geom
                                    FROM 
                                        (SELECT 
                                            ST_Transform(ST_GeomFromText(unnest(string_to_array('{wkt_to_postgres}', '$$$')), 4326), 32644) AS geom
                                        ) t
                                ) a,
                                roads	
                            WHERE
                                ST_DWithin(a.geom, roads.wkb_geometry, 5000)
                            ) d
                        ) e
                    WHERE e.rank = 1
                    ;
                    """
            nearest_road_data = querydb(nearest_road_query, transportdb_pool_obj)
            road_df = pd.DataFrame(nearest_road_data).set_index('input_id')

            df = pd.concat([a_df, road_df], axis=1)
            
            
            response_to_send['data'] = df.to_dict('records')
        else:
            response_to_send["usage"] = "the API should be supplied with parameters ?xy=X,Y|X1,Y1|X2,Y2;..."
        
        return Response(response_to_send)

def geocoder(data):
    pass

def reverse_geocoder(APIView):
    pass
